import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parno_ui/parno_ui.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        body: SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            LightTextBox(
              icon: Icon(CupertinoIcons.person),
              width: MediaQuery.of(context).size.width * 0.8,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
            ),
            LightTextBox(
              icon: Icon(CupertinoIcons.lock),
              width: MediaQuery.of(context).size.width * 0.8,
              obscureText: true,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5.0),
            ),
            LightTextButton('Log In', onPressed: () => null),
          ],
        ),
      ),
    ));
  }
}
