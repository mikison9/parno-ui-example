import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parno_ui/parno_ui.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                GestureDetector(
                  child: LightCard(
                    child: Icon(
                      CupertinoIcons.arrow_left,
                      color: Colors.white,
                    ),
                  ),
                  onTap: () => showFrostedDialog(
                      context: context,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          LightCard.text('Nvm třeba odstranit'),
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                          ),
                          LightCard.text('Přejmenovat'),
                          Padding(
                            padding: EdgeInsets.only(top: 8.0),
                          ),
                          LightCard.text('aaaaaaaaaaaaa'),
                        ],
                      )),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                ),
                LightCard(
                  width: 200,
                  child: roundedImage(Image.network(
                    'https://edit.org/images/cat/book-covers-big-2019101610.jpg',
                  )),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8.0),
                ),
                LightTextButton(
                  'Jeff',
                  onPressed: () => null,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
